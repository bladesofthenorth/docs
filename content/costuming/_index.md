---
title: "Costuming"
date: 2021-03-06T13:15:03+11:00
draft: false
weight: 20
---

Many Northies choose to buy their outfit ("soft kit") from [Burgschneider](https://burgschneider.eu/) . The North as a group, can order from Burgschneider and recive a (PERCENTAGE) discount. However, the North will charge (PERCENTAGE), with the extra 10% being contributing to the North [funds](https://www.facebook.com/groups/238339366208769) 

# Costume Approach

This document is designed to assist members of the North with familiarizing themselves with the unique aesthetics of the different themes within the warband. This document is not designed to rigidly tell people how they must dress, but to simply be an inspiration for members to develop their own kit.

When Swordcraft changed the setting of the game to Warhammer themed the North chose to stay mainly Viking and take inspiration from the Norscan armies of early editions. Over time we have worked on our interpretations and expanded our lore to include members from Kislev and the Northern Empire. The RP reason behind the North is a collection of members from different tribes of Norsca uniting under one banner with some Kislevs and Northern Empire to go South and find new fortunes in the new world of the Border Princes.

The North’s colours are Blue and Grey and we encourage everyone to wear them prominently in their kit, however neutral colours and accent/tribe colours can add to this while still keeping everything in theme. The North has been together IC for just over 10 years so it’s entirely possible for your character to dress themselves with pieces from other tribes or cultures.

Note: The Warhammer is a world that has been portrayed for over 30 years in countless books and role-playing, battle, board, and computer games. A world that is often inconsistent even within one edition and the textbooks have often contradicted each other. Although later editions of the Warhammer books have Norscans as exclusively Chaos armies, the original editions they are allies to the Empire against Chaos.


# Best Practices

Although every kit loadout is different, there are a number of essential items that are the base layer for making any kit meet minimum standards. Here is a list of things you should look at having in your kit:

### Minimum Outfit:
Shirt
Trousers, skirt, or dress
Shoes

### Outfit Extras
Headgear
Outwear

### Personal Item Extras:
Cup, plate, bowl, and cutlery
Wineskin/gourd/water container

Remember that as we live in Australia and the climate of the LARPs we attend can change rapidly. You may have a day of 40° weather followed by a night of -2°. It could be burning sunlight one moment and then a torrential storm. If you are attending an event that goes over multiple days, prepare for any kind of weather and have changes of clothes.
 
# Basic Features Of Outfits 
<img width="375" height="250" alt=Tablet Weave src="tabletweave">

Tablet Weaves

One of the items that is very common for Norscans and northern cultures are tablet weaves. These can be worn around your waist as a belt, used as a headband, or they can be sewn onto clothing as trim. There are many different patterns and sizes of tablet weaves and they can be simple to make by hand. It is also a good way to include some highlighting colour into your costume while still maintaining Blue/Grey and neutrals as your main colours.
<img style="float: right;" width="374" height="250" alt=Hammer Necklace src="hammer_neckpeice">

Religious Symbols  

Remember that the Old World is polytheistic – even if a person worships primarily a particular god, they acknowledge and usually respect the whole pantheon.
The Star of Chaos is a symbol forbidden in practically the whole world, and wearing it may end in your character being put to death. Although in later editions Norsca has converted to Chaos, the setting we are based on are wary of Chaos and fight against the corruption of the dark gods and any beings who follow them.

<img width="297" height="250" alt=Verious Religous Pendants src="pendants">


