---
title: "Main page"
date: 2021-03-06T13:15:03+11:00
draft: false
weight: 40
---

You can find examples of how this would look on [this page](https://bladesofthenorth.gitlab.io/docs/General/) 
that contains much of the North general guide, or on [this page](https://bladesofthenorth.gitlab.io/docs/costuming/), which contains some of the Costuming Guide. 

Both have examples of images, and [the Costuming page](https://bladesofthenorth.gitlab.io/docs/costuming/) 
has more links. The other pages are empty for this mock-up.
