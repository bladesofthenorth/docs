---
title: "North Main Page"
date: 2021-03-06T13:15:03+11:00
draft: false
weight: 10
---

# Who are the North?
The North is a LARP Warband based out of the Melbourne Swordcraft Chapter. Founded in (YEAR) by (NAMES), the North has been a family orientated warband, with great diversity and strong support for the LGBTQI+ community. As of 2021, the North average approximately 30 players a night, with approximately 80+ active members.

![Photo of the North](bladesgrouppic.jpg)

## The North’s Theme
The North has been a consistent presence in the LARPing community as a Viking, Pirate and Raider inspired warband. As the game evolved and the community started to expand bringing exclusive pirate and raider themed warbands, the North progressed towards simplifying its theme, to purely Vikings.

However with the change to Warhammer Inspiration for Swordcraft, the North has taken on the thematics of Norsca, Warhammer’s equivalent to vikings.


## North Origin

Norsca has always had a love/hate relationship with Chaos, whether it is by invasion of marauders and beasts from the Chaos Wastes, or by people praying to the Dark Gods to gain their powers.

While worship of darker gods is not uncommon in Norsca, not all of it is for personal gain or with evil intent. That being said the effects and taint of Chaos is noticeable in most of Norsca and as such those who reject Chaos tend to work together.

Originating from 5 of the least Chaos infected major tribes in Norsca, 6 Jarls decided to form a mercenary and mercantile warband. After recruiting additional members from Kislev and Ulricans, they set up a base camp called Nyrheim on the island of Eldvik in the Kuldevind Isles to train the members and to escape the Chaos that was seeping into the mainland of Norsca. While camped together they decided to name their warband “The North”.

After 10 years and multiple trips into the south, the main force of the warband travelled to the lands of the Border Princes and set up a camp outside the Duchy of Sudenburg to take part in the emerging new world and became known as The Blades of the North. Nearby another town named Kriegstor was being formed and so a portion of the warband set up camp there and became known as the Wolves of the North.

Both chapters work together to fight against Chaos while still attempting to turn a profit of coin and/or gain reputation either on the battlefield or through trade.

(update required re - wolves of middenheim)

(Insert photo here)


## North Location & Roleplaying

The Kuldevind Isles is the IC location of the North, where the warband and it’s inhabitants live when not campaigning in the Duchy of Sudenberg. This map is used as a reference point for characters who wish to travel around the island and interact with players on a digital platform via the North RP facebook group to further develop their character.



The link to the North RP page can be found below - all interactions on the page are done in character.

The North RP Facebook Group

If you are new to text based roleplaying and wish to learn more, please follow the link below for a basic how-to guide.

North Roleplaying Guide
Character Creation

Like many video game RPGs and tabletop games, one of the most daunting yet exciting things is creating a character. When creating a character in an environment or world that is already established, there are certain limitations on how to name your character and build them from the ground up. Below are some guidelines on creating a character for The North.

Name -
First name
Members of the North tend to rely on names typically associated with viking naming conventions. Some will rely on viking related TV Shows, Movies and Fantasy Name Generator to help come up with a name, others may look to translate their own name into the viking equivalent to create a closer connection. 

Some common advice to look out for when creating a name 
Having no more than 2-3 syllables in their first name. Some examples are Ivar, Yngvar, Sigtyr, Yosepr, Thyra, Einar, Gundar, Rangvald.
Try to avoid giving yourself a nickname straight off the bat, as your nickname will eventually come to you by either your actions throughout your adventures with the North.
Avoid edgy naming conventions. Edgebreaker, Woodsman, Shieldsmasher, Ironclaw are not ideal first names. 
Avoid names which would be commonly associated with other notable races and themes. Naming yourself Legolas or Hanzo as a Norscan simply doesn’t work.

Last name
As far as last names go, Vikings didn't know last names in the same way as many of us do now. Instead of an actual name, they would usually refer to people as son of or daughter of, hence why the last names all end in either son (son of) or dottir (daughter of). This is the exact same as the 'son' in modern day names like Michaelson for example. Having a last name is not mandatory, and you may simply avoid it if you do not wish to possess one.

Some common advice to look out for when creating a surname
Do you share any relations with other members of the North? Siblings?
Find something your character will like, have it translated into Norse via google translate and go from there.
Character Origin

When creating the backstory of your character, there are limitations of where your character can originate from, more specifically in the Warhammer Universe. These limitations are bound by how the Warhammer Universe operates, and can rarely be circumvented as that may diminish the experience for other players.

A short example would be a Norscan born in Athel Loren, a land belonging to the Elves. Current Warhammer lore would make that origin simply impossible.

## Race
The races most commonly found in the North are -
Norscan - Born and raised in Norsca, they are the fantasy equivalent of medieval vikings. Please follow this link for further reading - Norscans in the North
Kislevite - South of Norsca is the land of Kislev, the fantasy equivalent of Russian and Polish medieval warriors. Please follow this link for further reading - Kislevs in The North
Ulrican - TBA

Profession
One of the big pushes made during the transition to a Warhammer theme for Swordcraft is the importance of your characters career. When your character is not participating in a battle, what do they do in their spare time? Are they a blacksmith? Carpenter? Herbalist? Do they wish to run a stall at a quest event and make some money on the side?

The following compendium has an extensive list of careers and professions within the Warhammer Universe, and some are restricted to certain races, locations and rankings. If you are unsure of what kind of career you wish to pursue, please consult with a senior member of the North.

WARNING - LARGE DOWNLOAD - NOT MOBILE DATA FRIENDLY
Warhammer Career Compendium


## Tribes
Within the North there are 5 notable tribes of influence, each providing their own thematic and behavioural contributions to the North as a whole. When selecting a tribe to receive inspiration from, please note that first and foremost, you are a member of the North. Your tribe does not segregate you from other tribes, and there is no exclusivity from each one. 
Each Tribe possesses a North specific guide, containing further information about the Tribes, their thematics and more that have been developed by the warband due to limitations within Warhammer’s various sources.

Vargs
The Vargs are a nomadic people that wander the most northern areas of Norsca. As nomads, home is where the heart is, and that home is amongst their fellow tribesmen. Vargs value camaraderie and have eachothers backs in the most dire of situations. Vargs are a culture/foraging focused tribe, rather than one of martial prowess. They worship Rhya and Taal almost exclusively, however this does not necessarily indicate all Vargs follow Rhya and Taal.

Some common professions found within the Vargs tribes are 
Skalds (Singer, Actor, Musician, Story Teller)
Hunter/Gatherer (Trapping, Hunting, Archery, Gathering)
Herbalism/Field Medicine (A focus point on healing types, due to nomadic nature)

Some common costume pieces found on a Varg are
Animal parts (Symbolic, less trophy orientated)
Writing/Reading materials (Books, Tomes, Scrolls)
Travel Packs (Backpacks, satchels)
Hunting tools (Ropes, Hooks, Hatchets, trapping equipment)

Further reading about the Vargs Tribe is linked below
Vargs in Warhammer



## North Ranking system

The North is divided with ranks which in turn carry responsibility and kit requirements.

The Summary of ranks are below -

Thralls - Tourers and people who want to join the warband are the rank of Thrall.
Bondsmen - Full members who are guided into being a safe and comfortable member of the community by their buddy (Kyn).
Karl - Full member who has finished their buddy system.
Gildr - Exceptional member who has a recognisable character within the warband and contributes to the wider warband.
Vaskr - Hero characters who assist in managing large group activities.
Kyn - Veterans who take on the responsibility of buddying up with Bondsmen.
Jarls - Warband Leaders.

Thralls
Thralls are new members to the North and more often than not, new to swordcraft. This rank is designed to welcome in new members and help them get orientated with not only the warband, but Swordcraft as a whole. Thralls carry no administrative responsibilities within the warband, however they are still bound by the Code of Conduct. During their time as a Thrall, they have access to a Training schedule to help develop their fundamental understanding of LARP combat, such as footwork, strikes, counting hits, etc. Once Thralls have completed certain prerequisites, they are ready to be moved on to the stage of Bondsmen.

Bondsmen
Upon promotion to Bondsmen, they are considered full time members of the Warband and have access to the Blades of the North Facebook Group, along with being able to vote in the leadership elections. A Thrall becomes a bondsmen when they are ‘bonded’ to a Kyn, a senior member of the North. The Kyn is to mentor the bondsmen and facilitate their development, ranging from learning advanced combat techniques with their weapon of choice, to understanding the North better in regards to lore, traditions and tactics. Once the Kyn believes the Bondsmen is ready, the Kyn will organise a set of pre-determined In character (IC) and Out of Character(OOC) trials. These trials will not only test the Bondsmen of their Combat skills, but their knowledge of the Warband.

